package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectSql {
    Properties property = new Properties();

    public ConnectSql(FileInputStream fis) throws IOException {
        property.load(fis);
    }

    public Connection getConnection(){
        Connection connection = null;

        try{
            connection = DriverManager.getConnection(
                    property.getProperty("connection.url")
            );
        }
        catch (SQLException e){
            System.err.println("Failed connection");
            e.printStackTrace();
        }
        return connection;
    }
}
